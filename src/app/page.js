'use client'
import React, {useState, useEffect} from 'react'
import Image from 'next/image'
import Navbar from './comps/Navbar'
// import styles from './page.module.css'

export default function Home() {

  const [datas, setData] = useState([])

  useEffect(()=>{
    getData()
  },[])

  const getData = async () => {
    try {
      const res = await fetch('http://localhost:8000/api/cat-package');
      if (!res.ok) {
        throw new Error('HTTP error! Status: ' + res.status);
      }
      const data = await res.json();
      setData(data);
      console.log('data', data)
    } catch (error) {
      console.error('Terjadi kesalahan:', error);
    }
  }


  return (
    <main>
      <h2>Content</h2>

    </main>
  )
}



