/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ['example.com', 'localhost:8000', 'localhost'], // Add your domain(s) here
      },

}

module.exports = nextConfig
